const getSum = (str1, str2) => {
  if ((typeof(str1) != "string") || (typeof(str2) != "string"))
  {
    return false;
  }

  if ((str1.toUpperCase() != str1.toLowerCase()) && (str2.toUpperCase() != str2.toLowerCase()))
  {
    return false;
  }

  return String(+str1 + +str2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0;
  let commentsCount = 0;

  listOfPosts.forEach(post => {
    if (post.author == authorName)
    {
      postsCount++;
    }

    if (Object.keys(post).includes("comments"))
    {
      post.comments.forEach(comment => {
        if (comment.author == authorName)
        {
          commentsCount++;
        }
      });
    }
  });
  
  return `Post:${postsCount},comments:${commentsCount}`;
};

const tickets = (people) => {
  let result = 'YES';
  const ticketPrice = 25;

  people.reduce((totalMoney, personMoney) => {
    if (totalMoney + ticketPrice < personMoney - ticketPrice)
    {
      result = 'NO';
      people.splice(1);
    }

    return ticketPrice * 2 + totalMoney - personMoney;
  }, 0);
  
  return result;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
